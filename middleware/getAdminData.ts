import { ImagesStore } from "@/stores/images";

export default defineNuxtRouteMiddleware(async (to, from) => {
  const imageStore = ImagesStore();

  await imageStore.GetImages()
});
