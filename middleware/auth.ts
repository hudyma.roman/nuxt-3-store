import { useCookie } from "#app";
import { UserStore } from "@/stores/user";
export default defineNuxtRouteMiddleware(async (to, from) => {
  const store = UserStore();
  const token = useCookie("token");
  try {
    if (!token.value) return abortNavigation();

    const { user } = await $fetch("/api/user/me", {
      headers: {
        Authorization: token.value || "",
      },
    });

    store.setUser(user);
  } catch (error) {
    console.log("error", error);
    token.value = null;
    store.setUser(null);
    return await navigateTo("/login");
  }
});
