import { defineNuxtPlugin } from '#app'
import vClickOutside from 'v-click-outside'

export default defineNuxtPlugin((app) => {
  app.vueApp.use(vClickOutside)
})
