import Toast, { PluginOptions } from "vue-toastification";
import "vue-toastification/dist/index.css";

const option: PluginOptions = {};

export default defineNuxtPlugin((app) => {
  app.vueApp.use(Toast, option);
});
