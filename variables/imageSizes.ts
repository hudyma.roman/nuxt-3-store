export const imagesSizeStandards = [
  { w: 125, h: 156, name: "micro" },
  { w: 400, h: 500, name: "small" },
  { w: 600, h: 750, name: "medium" },
  { w: 800, h: 1000, name: "large" },
];
export const format = "webp";
export const dirInsidePublic = "upload";
