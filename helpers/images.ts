import { Image } from "@prisma/client";

export const getImgSrc = (image: Image | undefined) => {
  if(!image) return ''
  return `${image.path}${image.fileName}_medium.${image.format}`;
};

export const getTime = (valueDate: Date) => {
  const date = new Date(valueDate);
  const localDate = date.toLocaleDateString();
  const localTime = date.toLocaleTimeString();
  return {
    string: `${localTime}, ${localDate}`,
    localDate,
    localTime,
  };
};
