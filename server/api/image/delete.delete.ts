import { unlink } from "fs";
import {
  imagesSizeStandards,
  format,
  dirInsidePublic,
} from "~~/variables/imageSizes";
import path from "path";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
export default defineEventHandler(async (event) => {
  try {
    const { id } = getQuery(event);
    if (!id) return createError("id is required");
    if (Array.isArray(id)) return createError("id sold be only one");

    const image = await prisma.image.findFirstOrThrow({
      where: {
        id,
      },
    });

    const staticPath = `${path.join(
      path.resolve(),
      "public",
      dirInsidePublic
    )}`;

    imagesSizeStandards.forEach((sizeObject) => {
      const filePath = path.join(
        staticPath,
        `${image.fileName}_${sizeObject.name}.${image.format}`
      );
      unlink(filePath, (error) => {

        if (error) {
          console.log(error);
          return createError("deleting file error");
        }
      });
    });

    const deletedImage = await prisma.image.delete({
      where: {
        id,
      },
    });
    return {
      image: deletedImage,
    };
  } catch (error: any) {
    sendError(event, error);
  }
});
