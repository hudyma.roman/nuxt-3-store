import path from "path";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
import joi from "joi";
import formidable from "formidable";
import sharp from "sharp";
import { imagesSizeStandards, format, dirInsidePublic } from "~~/variables/imageSizes";


export default defineEventHandler(async (event) => {


  try {
    const form = formidable();
    // використовуємо formidable для отримання файла\ файлів з request
    const { files, fields } = await new Promise<any>((resolve, reject) => {
      form.parse(event.req, (err, fields, files) => {
        if (err || !files.photo) {
          reject(createError({ message: "upload failed" ,statusCode: 404 }));
        }
        resolve({ files, fields });
      });
    });
    await joi.string().trim().required().min(3).label('name').validateAsync(fields?.name);

    // отримуємо url файла з поля photo з request який знаходиться в папці Temp в залежності від ОС
    const imageInput = files.photo.filepath;
    // створюємо рандомне name для файла
    const date = new Date();
    const imageNameCode = String(date.getTime());
    const image = await prisma.image.create({
      data: {
        format,
        name: fields?.name,
        fileName: imageNameCode,
        path: `/${dirInsidePublic}/`,
      },
    });
    // отримуємо url папки для статики
    const staticPath = `${path.join(
      path.resolve(),
      "public",
      dirInsidePublic
    )}`;
    let objectWithFiles = {} as any;
    // створюємо Array з Promice по запису файлів в public теку
    const promiseArray = imagesSizeStandards.map((e) => {
      // створюємо назву файла в залежності від imageNameCode та стандартів назв розмірів з imagesSizeStandards (поле name)
      const imageName = `${imageNameCode}_${e.name}.${format}`;
      objectWithFiles[e.name] = imageName;
      //створюємо повну path для файла
      const newPath = `${path.join(staticPath, imageName)}`;
      // повертаємо async function з обробкою (перетворення в webp формат, покращення якості, зміна розміру під стандарти з imagesSizeStandards ) файла
      return sharp(imageInput)
        .toFormat(format)
        .webp({
          quality: 100,
          lossless: true,
        })
        .resize({ width: e.w })
        .toFile(newPath);
    });
    // виконуємо всі запити для запису файлів під розміри одночасно
    await Promise.all(promiseArray);

    return {
      image,
    };
  } catch (error: any) {
    console.log(error);
    return createError({ statusCode: 400, message: error.message });
  }
});
