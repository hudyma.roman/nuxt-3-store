import jwt, { Secret } from "jsonwebtoken";
import joi from "joi";
import bcrypt from "bcryptjs";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

const JUser = joi.object({
  email: joi.string().email().trim().min(3).max(100).label('email'),
  password: joi.string().trim().min(8).max(100).label('password'),
});

export default defineEventHandler(async (event) => {
  try {
    const { email, password } = await readBody(event);

    await JUser.validateAsync({ email, password });

    const user = await prisma.user.findUniqueOrThrow({
      where: {
        email,
      },
    });
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch)
      return createError({ statusCode: 403, message: "User not found" });

    const token = jwt.sign(
      { id: user.id.toString() },
      process.env.TOKEN_SECRET as Secret,
      { expiresIn: "7 days" }
    );
    return {
      user: { email: user.email, name: user.name },
      token,
    };
  } catch (error: any) {
    return createError({ statusCode: 404, message: error.message });
  }
});
