import bcrypt from "bcryptjs";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
import joi from "joi";
import jwt, { Secret } from "jsonwebtoken";

const JUser = joi.object({
  email: joi.string().email().trim().min(3).max(100).label('email'),
  name: joi.string().trim().min(3).max(100).label('name'),
  password: joi.string().trim().min(8).max(100).label('password'),
});
export default defineEventHandler(async (event) => {
  try {
    const { email, password, name } = await readBody(event);

    await JUser.validateAsync({ email, name, password });

    const salt = Number(process.env.BCRYPT_SALT);
    const cryptPassword = await bcrypt.hash(password, salt);
    const user = await prisma.user.create({
      data: {
        email,
        name,
        password: cryptPassword,
      },
    });

    const token = jwt.sign(
      { id: user.id.toString() },
      process.env.TOKEN_SECRET as Secret,
      { expiresIn: "7 days" }
    );
    return {
      user: { email: user.email, name: user.name },
      token,
    };
  } catch (error: any) {
    return createError({ statusCode: 404, message: error.message });
  }
});
