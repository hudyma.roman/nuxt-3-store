import jwt, { Secret } from "jsonwebtoken";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default defineEventHandler(async (event) => {
  const tokenRaw = getRequestHeader(event, "Authorization");
  const token = tokenRaw?.replace("Bearer ", "");
  if (typeof token !== "string")
    return createError({ statusCode: 404, message: "token error" });
  const decoded: any = jwt.verify(token, process.env.TOKEN_SECRET as Secret);

  try {
    const user = await prisma.user.findUniqueOrThrow({
      where: {
        id: decoded.id,
      },
    });

    return {
      user: { email: user.email, name: user.name },
    };
  } catch (error: any) {
    return createError({ statusCode: 404, message: error.message });
  }
});
