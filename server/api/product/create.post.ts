import joi from "joi";
import { v4 as uuid4 } from "uuid"
import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();

const JProduct = joi.object({
  id: joi.string().trim(),
  title: joi.string().trim().min(3).max(100).required(),
  description: joi.string().trim().min(3).max(1000),
  count: joi.number().min(0).integer().required(),
  price: joi.number().min(0).integer().required(),
});

export default defineEventHandler(async (event) => {
  try {
    const { id, title, description, count, price, avatar } = await readBody(event);
    await JProduct.validateAsync({id, title, description, count, price,})

    const product = await prisma.product.upsert({
      where: {
        id: id || uuid4()
      },
      create: { title, description, count, price, avatar: {
        connect: {
          id: avatar
        }
      } },
      update: { title, description, count, price, avatar: {
        connect: {
          id: avatar
        }
      } }
    })


    return {
      product
    }
  } catch (error: any) {
    console.log(error);
    return createError({ statusCode: 400, message: error.message });
  }
})
