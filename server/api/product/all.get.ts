import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default defineEventHandler(async (event) => {
  try {
    const products = await prisma.product.findMany({
      include: {
        avatar: {
          select: {
            fileName: true,
            path: true,
            format: true
          }
        },
      },
      orderBy: {
        updatedAt: "desc",
      },
    });
    return {
      products,
    };
  } catch (error: any) {
    sendError(event, error);
  }
});
