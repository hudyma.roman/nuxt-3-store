import joi from "joi";
import { v4 as uuid4 } from "uuid";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

const JCategory = joi.object({
  id: joi.string().trim(),
  title: joi.string().trim().min(3).max(100).required(),
});

export default defineEventHandler(async (event) => {
  try {
    const { id, title } = await readBody(event);
    await JCategory.validateAsync({ id, title });

    const category = await prisma.category.upsert({
      where: {
        id: id || uuid4(),
      },
      create: { title },
      update: { title },
    });

    return {
      category
    }
  } catch (error: any) {
    console.log(error);
    return createError({ statusCode: 400, message: error.message });
  }
});
