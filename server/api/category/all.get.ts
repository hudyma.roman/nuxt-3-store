import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();

export default defineEventHandler(async (event) => {
  try {
    const categories = await prisma.category.findMany({
      orderBy: {
        updatedAt: 'desc'
      }
    })
    return {
      categories
    }
  } catch (error: any) {
    sendError(event, error)
  }
})
