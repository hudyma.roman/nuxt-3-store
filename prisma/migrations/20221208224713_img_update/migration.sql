/*
  Warnings:

  - Added the required column `fileName` to the `Image` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Image" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "fileName" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "path" TEXT NOT NULL,
    "format" TEXT NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);
INSERT INTO "new_Image" ("createdAt", "format", "id", "name", "path") SELECT "createdAt", "format", "id", "name", "path" FROM "Image";
DROP TABLE "Image";
ALTER TABLE "new_Image" RENAME TO "Image";
CREATE UNIQUE INDEX "Image_fileName_key" ON "Image"("fileName");
CREATE UNIQUE INDEX "Image_name_key" ON "Image"("name");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
