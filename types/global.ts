interface IUser {
  name: string;
  email: string;
}

interface ILoginArgument {
  email: string;
  password: string;
}

interface IRegistrationArgument {
  name: string;
  email: string;
  password: string;
}
