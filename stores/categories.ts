import { defineStore } from "pinia";
import { Category } from "@prisma/client";

export const useCategoriesStore = defineStore("categories", {
  state: () => ({ categories: null } as { categories: Category[] | null }),

  getters: {
    CATEGORIES: (state): Category[] | null => state.categories,
  },

  actions: {
    async GetCategories() {
      try {
        const { categories } = await $fetch("/api/category/all");
        this.categories = categories;
        return categories;
      } catch (error: any) {
        console.log(error);
        useToast().error("Ops! Categories request is failed");
      }
    },
  },
});
