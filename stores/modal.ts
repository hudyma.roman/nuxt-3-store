import { defineStore } from "pinia";

export const useModalStore = defineStore("modal", {
  state: () =>
    ({ modalState: false, fullscreenModalState: false } as {
      modalState: Boolean;
      fullscreenModalState: Boolean;
    }),

  getters: {
    MODAL_STATE: (state) => state.modalState,
    FULLSCREEN_MODAL_STATE: (state) => state.fullscreenModalState,
  },

  actions: {
    // simple modal actins
    ToggleModal() {
      this.modalState = !this.modalState;
    },
    CloseModal() {
      this.modalState = false;
    },
    OpenModal() {
      this.modalState = true;
    },

    // fullscreen modal actions
    ToggleFullscreenModal() {
      this.fullscreenModalState = !this.fullscreenModalState;
    },
    CloseFullscreenModal() {
      this.fullscreenModalState = false;
    },
    OpenFullscreenModal() {
      this.fullscreenModalState = true;
    },
  },
});
