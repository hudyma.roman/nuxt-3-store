import { defineStore } from "pinia";

interface User {
  name: string;
  email: string;
}

export const UserStore = defineStore("user", {
  state: () => ({ user: null } as { user: User | null }),

  getters: {
    USER: (state) => state.user,
  },

  actions: {
    setUser(value: User | null) {
      this.user = value;
    },
  },
});
