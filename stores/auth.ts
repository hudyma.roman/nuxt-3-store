import { defineStore } from "pinia";

export const useAuthStore = defineStore("auth", {
  state: () =>
    ({
      user: null,
      token: "",
    } as {
      user: IUser | null;
      token: string;
    }),

  getters: {
    USER: (state) => state.user,
  },

  actions: {
    async Login({ email, password }: ILoginArgument) {
      try {
        const { user, token } = await $fetch("/api/user/login", {
          method: "post",
          body: {
            email,
            password,
          },
        });

        this.user = user;
        this.token = token;
        useToast().success("Login success");
      } catch (error: any) {
        console.log(error);
        useToast().error("Login failed");
      }
    },
    async Registration({ name, email, password }: IRegistrationArgument) {
      try {
        const { user, token } = await $fetch("/api/user/create", {
          method: "post",
          body: {
            name,
            email,
            password,
          },
        });

        this.user = user;
        this.token = token;
        useToast().success("Registration success");
      } catch (error: any) {
        console.log(error);
        useToast().error("Registration failed");
      }
    },
  },
});


