import { defineStore } from "pinia";
import { User } from "@prisma/client";

export const useUsersStore = defineStore("users", {
  state: () => ({ users: null } as { users: User[] | null }),

  getters: {
    USERS: (state) => state.users,
  },

  actions: {
    async GetUsers() {
      try {
        const { users } = await $fetch("/api/user/all");
        this.users = users;
        return users;
      } catch (error: any) {
        console.log(error);
        useToast().error("Ops! Users request is failed");
      }
    },
  },
});
