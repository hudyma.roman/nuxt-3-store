import { defineStore } from "pinia";
import { Image } from "@prisma/client";

export const ImagesStore = defineStore("images", {
  state: () => ({ images: [] } as { images: Image[] }),

  getters: {
    IMAGES: (state) => state.images,
  },

  actions: {
    async GetImages() {
      try {
        const { images } = await $fetch("/api/image/all");
        this.images = images;
      } catch (error) {
        console.log(error);
      }
    },
  },
});
