import { defineStore } from "pinia";

export const AlertStore = defineStore("alert", {
  state: () => ({ alertState: false } as { alertState: Boolean }),

  getters: {
    ALERT_STATE: (state) => state.alertState,
  },

  actions: {
    CloseAlert() {
      this.alertState = false;
    },
    OpenAlert() {
      this.alertState = true;
    },
  },
});
