import { defineNuxtConfig } from "nuxt/config";
import svgLoader from "vite-svg-loader";

export default defineNuxtConfig({
  css: ["~/assets/css/main.css", "~/assets/scss/global.scss"],
  modules: [
    "@pinia/nuxt",
    // '@nuxt/devtools'
  ],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  vite: {
    plugins: [svgLoader()],
  },
  build: {
    transpile: ["vue-toastification"],
  },
});
